import { createStore } from 'vuex'

export default createStore({
  state: {
    darkMode: false,
    restart: false,
  },
  getters: {
    MODE: state => {
      return state.darkMode;
    },
    RESTART: state => {
      return state.restart;
    },
  },
  mutations: {
    SET_MODE: (state, payload) => {
      state.darkMode = payload;
    },
    SET_RESTART: (state, payload) => {
      state.restart = payload;
    },
  },
  actions: {
    SET_MODE: (context, payload) => {
      context.commit('SET_MODE', payload);
    },
    SET_RESTART: (context, payload) => {
      context.commit('SET_RESTART', payload);
    },
  },
  modules: {
  }
})
